import React, { useRef } from "react";
import CustomNavLink from "./CustomNavLink";
import { Container } from "@mui/system";
import Typography from "@mui/material/Typography";
import { AppBar, Toolbar, Switch, FormControlLabel } from "@mui/material";
import LightModeIcon from "@mui/icons-material/LightMode";
import DarkModeIcon from "@mui/icons-material/DarkMode";
import { useAppDispatch, useAppSelector } from "../../store/store";
import { Id, toast } from "react-toastify";
import { setMode } from "../../store/dark-mode-slice";

const MainNavigation = () => {
  const compareCount: number = useAppSelector(
    (state) => state.pokemon.pokemonsToCompare
  ).length;

  const toastId = useRef<Id | null>(null);
  const handleOpenSnackbar = () => {
    if (!toast.isActive(toastId.current as number | string)) {
      toastId.current = toast("Please, choose 2 Pokemons to compare!", {
        icon: "😠",
      });
    }
  };

  const isDarkMode = useAppSelector((state) => state.darkMode.darkMode);
  const dispatch = useAppDispatch();
  const handleModeChange = () => {
    dispatch(setMode(!isDarkMode));
  };

  return (
    <AppBar position="static">
      <Toolbar>
        <img
          alt={""}
          src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Pok%C3%A9_Ball_icon.svg/1200px-Pok%C3%A9_Ball_icon.svg.png"
          style={{ width: 50, height: 50 }}
        />
        <Typography variant="h4" sx={{ fontFamily: "Slackey" }}>
          POKEMONS
        </Typography>
        <Container>
          <CustomNavLink to="/" disabled={false}>
            Home
          </CustomNavLink>
          {/* <CustomNavLink to="/details">Details</CustomNavLink> */}
          <CustomNavLink
            to="/compare"
            disabled={compareCount !== 2}
            onOpenSnackbar={handleOpenSnackbar}
          >
            Compare
          </CustomNavLink>
        </Container>
        <FormControlLabel
          control={
            <Switch
              checked={isDarkMode}
              onChange={handleModeChange}
              inputProps={{ "aria-label": "controlled" }}
            />
          }
          label={
            !isDarkMode ? (
              <LightModeIcon
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              />
            ) : (
              <DarkModeIcon
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              />
            )
          }
        />
      </Toolbar>
    </AppBar>
  );
};

export default MainNavigation;
