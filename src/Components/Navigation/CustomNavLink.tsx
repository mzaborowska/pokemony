import React from "react";
import { NavLink } from "react-router-dom";

type NavLinkProps = {
  to: string;
  disabled: boolean;
  children: string;
  onOpenSnackbar?: () => void;
};
const CustomNavLink = ({
  to,
  children,
  disabled,
  onOpenSnackbar,
}: NavLinkProps) => {
  const handleClick = (e: React.MouseEvent<HTMLElement>) => {
    if (disabled) {
      if (onOpenSnackbar) {
        onOpenSnackbar();
      }
      e.preventDefault();
    }
  };
  return (
    <NavLink
      to={to}
      style={({ isActive }) => ({
        padding: "10px 10px 5px",
        fontSize: 20,
        color: disabled ? "rgba(255,255,255,.6)" : "#fff",
        textDecoration: "none",
        borderBottom: isActive ? "2px solid white" : "",
        cursor: disabled ? "not-allowed" : "pointer",
        fontFamily: "Slackey",
      })}
      onClick={handleClick}
    >
      {children}
    </NavLink>
  );
};

export default CustomNavLink;
