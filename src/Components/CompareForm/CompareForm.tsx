import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import React from "react";
import { pokemonWithDetailType } from "../../types/pokemonTypes";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

type myProps = {
  data: pokemonWithDetailType;
};
const fullWidthTypes = ["name", "types", "abilities"];
const smallWidthTypes = ["name", "types", "abilities", "id", "imgUrl", "moves"];
const CompareForm = ({ data }: myProps) => {
  const getAnimalsLongContent = (data: any) => {
    return Object.keys(data).map((key) => {
      if (fullWidthTypes.includes(key)) {
        const item = data[key];
        return (
          <Grid item xs={12}>
            <TextField
              key={key}
              id="filled-disabled"
              disabled
              label={key[0].toUpperCase() + key.slice(1)}
              defaultValue={Array.isArray(item) ? item.join(", ") : item}
              sx={{ mb: 2, width: "100%" }}
            />
          </Grid>
        );
      }
      if (!smallWidthTypes.includes(key)) {
        const item = data[key];
        return (
          <Grid item xs={12} md={6}>
            <TextField
              key={key}
              id="filled-disabled"
              disabled
              label={key[0].toUpperCase() + key.slice(1)}
              defaultValue={Array.isArray(item) ? item.join(", ") : item}
              sx={{ mb: 2, width: "100%" }}
            />
          </Grid>
        );
      }
      if (key === "moves") {
        const item = data[key];
        return (
          <Grid item xs={12}>
            <Accordion>
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                <Typography>Moves</Typography>
              </AccordionSummary>
              <AccordionDetails>
                <Typography>{item.slice().join(", ")}</Typography>
              </AccordionDetails>
            </Accordion>
          </Grid>
        );
      }
    });
  };

  return (
    <>
      <Grid
        container
        justifyContent="center"
        alignItems="center"
        columnSpacing={2}
      >
        {data && (
          <img
            alt={""}
            style={{ marginBottom: 4, width: 150, height: 150 }}
            src={data.imgUrl.dream}
          />
        )}
        {data ? getAnimalsLongContent(data) : ""}
      </Grid>
    </>
  );
};

export default CompareForm;
