import React from "react";
import {
  TextField,
  Grid,
  Typography,
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { pokemonWithDetailType } from "../../types/pokemonTypes";

type propsType = {
  pokemon: pokemonWithDetailType;
};

const DetailsForm = ({ pokemon }: propsType) => {
  return (
    // <Container sx={{ mt: 4 }}>
    <Grid container columnSpacing={4}>
      <Grid item xs={12} sx={{ p: 0 }}>
        <Typography
          variant="h2"
          sx={{ mb: 4, textAlign: "center", fontFamily: "Slackey" }}
        >
          {pokemon.name} Details
        </Typography>
      </Grid>
      <Grid container xs={8}>
        <TextField
          disabled
          id="outlined-disabled"
          label="Name"
          defaultValue={pokemon.name}
          fullWidth
          sx={{ mb: 3 }}
        />
        <Grid container spacing={2}>
          <Grid item xs={6}>
            <TextField
              disabled
              id="outlined-disabled"
              label="Height"
              defaultValue={pokemon.height}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              disabled
              id="outlined-disabled"
              label="Weight"
              defaultValue={pokemon.weight}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
        </Grid>
        <Grid container columnSpacing={2}>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Hp"
              id="outlined-disabled"
              defaultValue={pokemon.hp}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Attack"
              id="outlined-disabled"
              defaultValue={pokemon.attack}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Special Attack"
              id="outlined-disabled"
              defaultValue={pokemon.specialAttack}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Speed"
              id="outlined-disabled"
              defaultValue={pokemon.speed}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Defense"
              id="outlined-disabled"
              defaultValue={pokemon.defense}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
          <Grid item xs={4}>
            <TextField
              disabled
              label="Special Defense"
              id="outlined-disabled"
              defaultValue={pokemon.specialDefense}
              fullWidth
              sx={{ mb: 3 }}
            />
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled
            id="outlined-disabled"
            label={pokemon.types.length === 1 ? "Type" : "Types"}
            defaultValue={pokemon.types.slice().join(", ")}
            fullWidth
            sx={{ mb: 3 }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            disabled
            label="Skills"
            id="outlined-disabled"
            defaultValue={pokemon.abilities.slice().join(", ")}
            fullWidth
            sx={{ mb: 3 }}
          />
        </Grid>
        <Accordion>
          <AccordionSummary expandIcon={<ExpandMoreIcon />}>
            <Typography>Moves</Typography>
          </AccordionSummary>
          <AccordionDetails>
            <Typography>{pokemon.moves.slice().join(", ")}</Typography>
          </AccordionDetails>
        </Accordion>
      </Grid>
      <Grid
        container
        xs={4}
        justifyContent="center"
        alignItems="center"
        sx={{ position: "relative" }}
      >
        <img
          src={pokemon.imgUrl?.dream}
          alt={pokemon.name}
          style={{
            width: "100%",
            objectFit: "contain",
            position: "absolute",
          }}
        />
      </Grid>
    </Grid>
    // </Container>
  );
};

export default DetailsForm;
