import { Chip, Grid } from "@mui/material";
import RemainingItems from "./RemainingItems";
import React, { useState } from "react";
import AbilitiesList from "./AbilitiesList";
import Modal from "../UI/Modal";
import { pokemonWithDetailType } from "../../types/pokemonTypes";

type abilitiesCellProps = {
  abilities: string[];
  pokemon: pokemonWithDetailType;
};
const AbilitiesCell = ({ abilities, pokemon }: abilitiesCellProps) => {
  const [canShowAbilities, setCanShowAbilities] = useState(false);
  const handleToggleOpenAbilities = () => {
    setCanShowAbilities((prev) => !prev);
  };

  return (
    <>
      <Modal open={canShowAbilities} onClose={handleToggleOpenAbilities}>
        <AbilitiesList
          name={pokemon?.name}
          abilities={pokemon?.abilities}
          img={pokemon?.imgUrl.front}
        />
      </Modal>
      <Grid container alignItems="center">
        <Chip
          label={abilities[0][0].toUpperCase() + abilities[0].slice(1)}
          size="medium"
          sx={{
            bgcolor: "primary.main",
            color: "#fff",
            fontSize: 16,
            height: 40,
            borderRadius: 20,
          }}
          onClick={handleToggleOpenAbilities}
        />
        {abilities.length > 1 && (
          <RemainingItems
            count={abilities.length - 1}
            name={abilities}
            onClick={handleToggleOpenAbilities}
          />
        )}
      </Grid>
    </>
  );
};
export default AbilitiesCell;
