import {
	Avatar,
	Grid,
	List,
	ListItem,
	ListItemText,
	Typography,
} from "@mui/material";
import React from "react";

type propTypes = {
	name: string | undefined;
	abilities: string[] | undefined;
	img: string | undefined;
};
const AbilitiesList = ({ name = "", abilities, img }: propTypes) => {
	return (
		<Grid container justifyContent="center">
			<Grid item xs={12}>
				<Typography variant="h4" sx={{ mb: 2, textAlign: "center" }}>
					{name} Abilities
				</Typography>
			</Grid>
			<Grid container>
				<Grid item xs={8}>
					<List>
						{abilities &&
							abilities.map((a) => (
								<ListItem key={a}>
									<ListItemText>
										{a[0].toUpperCase() + a.slice(1)}
									</ListItemText>
								</ListItem>
							))}
					</List>
				</Grid>
				<Grid
					container
					xs={4}
					justifyContent="center"
					alignItems="center">
					<Avatar
						src={img}
						alt={name}
						sx={{ width: 100, height: 100 }}
					/>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default AbilitiesList;
