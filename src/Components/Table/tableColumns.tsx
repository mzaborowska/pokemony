import { createColumnHelper } from "@tanstack/react-table";
import { pokemonWithDetailType } from "../../types/pokemonTypes";
import { keyframes, styled } from "@mui/material";
import React from "react";
import ActionButtons from "./ActionButtons";
import AbilitiesCell from "./AbilitiesCell";
import TypesCell from "./TypesCell";

const ImageContainer = styled("div")({
  position: "relative",
  perspective: 900,
  width: 96,
  height: 96,
});
const rotateFront = keyframes`
  0% {
    opacity: 1;
    transform: rotate(0)
  }
  30% {
    opacity: 1;
    transform: rotate(0)
  }
  35% {
    opacity: 0;
    transform: rotateY(180deg)

  }
  70% {
    opacity: 0;
    transform: rotateY(180deg)
  }
  75% {
    opacity: 1;
    transform: rotateY(0);
  }
  100% {
    opacity: 1;
    transform: rotateY(0);
  }
`;
const rotateBack = keyframes`
  0% {
    opacity: 0;
    transform: rotateY(-180deg);

  }
  30% {
    opacity: 0;
    transform: rotateY(-180deg);
  }

  35% {
    opacity: 1;
    transform: rotate(0);

  }
  70% {
    opacity: 1;
    transform: rotate(0)
  }
  75% {
    opacity: 0;
    transform: rotateY(-180deg);
  }
  100% {
    opacity: 0;
    transform: rotateY(-180deg);
  }
`;
const Image = styled("img")({
  position: "absolute",
  top: 0,
  left: 0,
  animation: `${rotateFront} 6s infinite  `,
  "&:nth-of-type(2)": {
    animation: `${rotateBack} 6s  infinite  `,
  },
});

const columnHelper = createColumnHelper<pokemonWithDetailType>();

const columns = [
  columnHelper.accessor("imgUrl", {
    header: "Image",
    cell: (info) => (
      <ImageContainer>
        <Image src={info.getValue().front} alt={info.row.original.name}></Image>
        <Image src={info.getValue().back} alt={info.row.original.name}></Image>
      </ImageContainer>
    ),
  }),
  columnHelper.accessor("name", {
    header: "Name",
    cell: (info) => info.getValue()[0].toUpperCase() + info.getValue().slice(1),
  }),
  columnHelper.accessor("types", {
    header: "Types",
    cell: (info) => (
      <TypesCell types={info.getValue()} pokemon={info.row.original} />
    ),
  }),
  columnHelper.accessor("abilities", {
    header: "Abilities",
    cell: (info) => (
      <AbilitiesCell abilities={info.getValue()} pokemon={info.row.original} />
    ),
  }),
  columnHelper.accessor("id", {
    header: "Actions",
    cell: (info) => {
      return <ActionButtons id={info.getValue()} />;
    },
  }),
];

export default columns;
