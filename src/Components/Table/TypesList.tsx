import {
  Avatar,
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Typography,
} from "@mui/material";
import React from "react";
import TypeAvatar from "./TypeAvatar";

type propTypes = {
  types: string[] | undefined;
  name: string | undefined;
  img: string | undefined;
};
const TypesList = ({ types, name, img }: propTypes) => {
  return (
    <Grid container justifyContent="center">
      <Grid item xs={12}>
        <Typography variant="h4" sx={{ mb: 2, textAlign: "center" }}>
          {name} Types
        </Typography>
      </Grid>
      <Grid item xs={8}>
        <List
          sx={{
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
          }}
        >
          {types &&
            types.map((t) => (
              <ListItem key={t}>
                <ListItemAvatar>
                  <TypeAvatar type={t} onClick={() => {}} />
                </ListItemAvatar>
                <ListItemText primary={t} />
              </ListItem>
            ))}
        </List>
      </Grid>
      <Grid container xs={4} justifyContent="center" alignItems="center">
        <Avatar src={img} alt={name} sx={{ width: 100, height: 100 }} />
      </Grid>
    </Grid>
  );
};

export default TypesList;
