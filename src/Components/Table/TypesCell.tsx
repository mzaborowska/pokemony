import { Grid } from "@mui/material";
import TypeAvatar from "./TypeAvatar";
import RemainingItems from "./RemainingItems";
import React, { useState } from "react";
import { pokemonWithDetailType } from "../../types/pokemonTypes";
import TypesList from "./TypesList";
import Modal from "../UI/Modal";

type typesCellProps = {
  types: string[];
  pokemon: pokemonWithDetailType;
};
const TypesCell = ({ types, pokemon }: typesCellProps) => {
  const [canShowTypes, setCanShowTypes] = useState(false);
  const handleToggleOpenTypes = () => {
    setCanShowTypes((prev) => !prev);
  };

  return (
    <>
      <Modal open={canShowTypes} onClose={handleToggleOpenTypes}>
        <TypesList
          types={pokemon?.types}
          name={pokemon?.name}
          img={pokemon?.imgUrl.front}
        />
      </Modal>
      <Grid container alignItems="center">
        <TypeAvatar
          type={types[0]}
          onClick={handleToggleOpenTypes}
          pointer={true}
        />
        {types.length > 1 && (
          <RemainingItems
            count={types.length - 1}
            name={types}
            onClick={handleToggleOpenTypes}
          />
        )}
      </Grid>
    </>
  );
};
export default TypesCell;
