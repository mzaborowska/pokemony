import { Button, ButtonGroup, Menu, MenuItem } from "@mui/material";
import { NavLink } from "react-router-dom";
import { pokemonWithDetailType } from "../../types/pokemonTypes";
import React, { useRef, useState, MouseEvent } from "react";
import { useAppDispatch, useAppSelector } from "../../store/store";
import { addPokemonToCompare } from "../../store/pokemon-action";
import { Id, toast } from "react-toastify";
import MoreVertIcon from "@mui/icons-material/MoreVert";

type buttonsProps = {
  id: number;
};
const ActionButtons = ({ id }: buttonsProps) => {
  const pokemonsToCompare = useAppSelector(
    (state) => state.pokemon.pokemonsToCompare
  );
  const dispatch = useAppDispatch();

  const toastId = useRef<Id | null>(null);
  const handleAddToCompare = (pokemonId: number) => {
    dispatch(addPokemonToCompare(pokemonId));
    if (!toast.isActive(toastId.current as number | string)) {
      toastId.current = toast(`Added Pokemon for comparison`, {
        toastId: `addToCompare${pokemonId}`,
        icon: "👌",
      });
    }
  };

  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const [openMenu, setOpenMenu] = useState(false);
  const handleOpenMenu = (event: MouseEvent<HTMLButtonElement>) => {
    setOpenMenu(true);
    setAnchorEl(event.currentTarget);
  };
  const handleCloseMenu = () => {
    setOpenMenu(false);
  };
  return (
    <>
      <Button onClick={handleOpenMenu} id="basic-button">
        <MoreVertIcon />
      </Button>
      <Menu
        open={openMenu}
        MenuListProps={{
          "aria-labelledby": "basic-button",
        }}
        onClose={handleCloseMenu}
        anchorEl={anchorEl}
      >
        <MenuItem onClick={handleCloseMenu}>
          <Button
            component={NavLink}
            style={{
              color: "#fff",
              textDecoration: "none",
            }}
            to={`/details/${id}`}
          >
            Details
          </Button>
        </MenuItem>
        <MenuItem onClick={handleCloseMenu}>
          <Button
            sx={{ color: "#fff" }}
            onClick={() => handleAddToCompare(id)}
            disabled={
              pokemonsToCompare.length === 2 ||
              pokemonsToCompare.filter(
                (p: pokemonWithDetailType) => p.id === id
              ).length !== 0
            }
          >
            Compare
          </Button>
        </MenuItem>
      </Menu>
    </>
  );
};
export default ActionButtons;
