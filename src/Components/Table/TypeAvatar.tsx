import React from "react";
import WhatshotIcon from "@mui/icons-material/Whatshot";
import GrassIcon from "@mui/icons-material/Grass";
import AirIcon from "@mui/icons-material/Air";
import WaterDropIcon from "@mui/icons-material/WaterDrop";
import BugReportIcon from "@mui/icons-material/BugReport";
import ScienceIcon from "@mui/icons-material/Science";
import ElectricBoltIcon from "@mui/icons-material/ElectricBolt";
import AcUnitIcon from "@mui/icons-material/AcUnit";

import { Avatar, Tooltip } from "@mui/material";

type TypeAvatarType = {
  type: string;
  onClick: () => void;
  pointer?: boolean;
};
const TypeAvatar = ({ type, onClick, pointer }: TypeAvatarType) => {
  return (
    <Tooltip title={type}>
      <Avatar
        onClick={() => onClick()}
        sx={{
          cursor: pointer ? "pointer" : "",
          bgcolor:
            type === "fire"
              ? "#EE8130"
              : type === "water"
              ? "#6390F0"
              : type === "poison"
              ? "#A33EA1"
              : type === "flying"
              ? "#A98FF3"
              : type === "grass"
              ? "#7AC74C"
              : type === "bug"
              ? "#A6B91A"
              : type === "electric"
              ? "#F7D02C"
              : type === "ice"
              ? "#96D9D6"
              : type === "ground"
              ? "#E2BF65"
              : type === "fighting"
              ? "#C22E28"
              : "#A8A77A",
        }}
      >
        {type === "fire" ? (
          <WhatshotIcon />
        ) : type === "water" ? (
          <WaterDropIcon />
        ) : type === "poison" ? (
          <ScienceIcon />
        ) : type === "flying" ? (
          <AirIcon />
        ) : type === "grass" ? (
          <GrassIcon />
        ) : type === "bug" ? (
          <BugReportIcon />
        ) : type === "electric" ? (
          <ElectricBoltIcon />
        ) : type === "ice" ? (
          <AcUnitIcon />
        ) : type === "ground" ? (
          "G"
        ) : type === "fighting" ? (
          "F"
        ) : (
          "N"
        )}
      </Avatar>
    </Tooltip>
  );
};

export default TypeAvatar;
