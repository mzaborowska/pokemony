import React, { useEffect, useMemo, useState } from "react";
import {
  flexRender,
  getCoreRowModel,
  useReactTable,
} from "@tanstack/react-table";
import {
  Box,
  CircularProgress,
  Grid,
  InputAdornment,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@mui/material";

import { useAppDispatch, useAppSelector } from "../../store/store";
import {
  getAllPokemons,
  getAllPokemonsCount,
} from "../../store/pokemon-action";

import SearchIcon from "@mui/icons-material/Search";
import columns from "./tableColumns";
import { Id, toast } from "react-toastify";

const tablePaginationOptions = [5, 10, 20, 50, 100];

const HomeTable = () => {
  const dispatch = useAppDispatch();

  const [page, setPage] = useState<number>(0);
  const [rowsPerPage, setRowsPerPage] = useState<number>(5);

  const customId = "notFound";
  const toastId = React.useRef<Id | null>(null);
  useEffect(() => {
    dispatch(getAllPokemonsCount());
    dispatch(getAllPokemons(rowsPerPage, page * rowsPerPage))
      .then()
      .catch((err) => {
        if (!toast.isActive(toastId.current as number | string)) {
          toastId.current = toast(
            "Something went wrong! Pokemons data not found ",
            { icon: "🤷", toastId: customId }
          );
        }

        console.log("home", err);
      });
  }, [page, rowsPerPage]);

  const pokemonsCount = useAppSelector(
    (state) => state.pokemon.allPokemonsCount
  );
  const pokemonsWithDetailsList = useAppSelector(
    (state) => state.pokemon.pokemonsWithDetails
  );
  const loadingPokemonsData = useAppSelector(
    (state) => state.pokemon.loadingPokemonsData
  );

  //pokemons after search
  const [searchedPokemon, setSearchedPokemon] = useState<string>("");

  const pokemonsToShow = useMemo(() => {
    return pokemonsWithDetailsList.filter((p) =>
      p.name.toLowerCase().includes(searchedPokemon.toLowerCase())
    );
  }, [searchedPokemon, pokemonsWithDetailsList]);

  const onRowsPerPageChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setSearchedPokemon("");
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const handleChangePae = (
    event: React.MouseEvent<HTMLButtonElement> | null,
    newPage: number
  ) => {
    setSearchedPokemon("");
    setPage(newPage);
  };
  const handleSearchedPokemonChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setSearchedPokemon(event.target.value);
  };

  const table = useReactTable({
    data: pokemonsToShow || [],
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  if (loadingPokemonsData)
    return (
      <Grid container justifyContent="center" p={16}>
        <CircularProgress />
      </Grid>
    );

  return (
    <>
      <Box
        sx={{
          width: "100%",
          position: "relative",
        }}
      >
        <TextField
          placeholder="Search..."
          color={"primary"}
          value={searchedPokemon}
          onChange={handleSearchedPokemonChange}
          sx={{
            width: "100%",
          }}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            ),
          }}
        />
      </Box>
      <Paper elevation={4} sx={{ mb: 8 }}>
        <TableContainer
          sx={{
            ml: "auto",
            mr: "auto",
          }}
        >
          <Table>
            <TableHead>
              {table.getHeaderGroups().map((headerGroup) => (
                <TableRow key={headerGroup.id}>
                  {headerGroup.headers.map((header) => (
                    <TableCell key={header.id}>
                      {flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                    </TableCell>
                  ))}
                </TableRow>
              ))}
            </TableHead>
            <TableBody>
              {table.getRowModel().rows.map((row) => (
                <TableRow key={row.id}>
                  {row.getVisibleCells().map((cell) => {
                    return (
                      <TableCell key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={tablePaginationOptions}
          component="div"
          count={pokemonsCount}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePae}
          onRowsPerPageChange={onRowsPerPageChange}
        />
      </Paper>
    </>
  );
};

export default HomeTable;
