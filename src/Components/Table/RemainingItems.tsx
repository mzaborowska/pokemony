import { Button, Tooltip } from "@mui/material";
import Avatar from "@mui/material/Avatar/Avatar";
import React from "react";

type RemainingItemsType = {
  count: number;
  name: string[];
  onClick: () => void;
};
const RemainingItems = ({ count, name, onClick }: RemainingItemsType) => {
  return (
    <Tooltip title={name.slice(1).join(", ")} onClick={() => onClick()}>
      <Avatar sx={{ width: 35, height: 35 }}>
        <Button sx={{ color: "#fff" }}>{count}+</Button>
      </Avatar>
    </Tooltip>
  );
};

export default RemainingItems;
