import React from "react";
import {
  Avatar,
  Box,
  IconButton,
  Grid,
  Typography,
  Button,
} from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../store/store";
import CloseIcon from "@mui/icons-material/Close";
import styled from "@emotion/styled";
import { removePokemonFromCompareSuccess } from "../../store/pokemon-slice";
import { toast } from "react-toastify";

interface themeInterface {
  palette: {
    primary: {
      main: string;
    };
  };
}

const AvatarBox = styled("div")({
  position: "relative",
});

const ChoosenPokemonsBox = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const choosenPokemons = useAppSelector(
    (state) => state.pokemon.pokemonsToCompare
  );

  const handleRemove = (id: number) => {
    dispatch(removePokemonFromCompareSuccess(id));
    toast("Removed Pokemon from comparation", {
      icon: "🙀",
    });
  };
  const handleCompareClick = () => {
    navigate("/compare");
  };

  return (
    <Grid
      container
      direction="column"
      justifyContent="center"
      alignContent="center"
      sx={{ my: 4 }}
    >
      <Box
        sx={{
          p: 2,

          minWidth: 250,
          border: 1,
          borderColor: "primary.main",
          borderRadius: 2,
        }}
      >
        {!choosenPokemons.length && (
          <Typography variant="body1" sx={{ textAlign: "center" }}>
            Choose 2 Pokemons to compare
          </Typography>
        )}
        <Grid
          container
          columnSpacing={2}
          justifyContent="center"
          direction="row"
        >
          {choosenPokemons.map((p) => (
            <Grid key={p.id} item justifyContent="center" alignItems="center">
              <AvatarBox>
                <Avatar
                  src={p.imgUrl.front}
                  sx={{
                    width: 100,
                    height: 100,
                  }}
                ></Avatar>
                <IconButton
                  sx={{
                    position: "absolute",
                    width: "fit-content",
                    top: -10,
                    right: -10,
                    p: 2,
                    color: (theme: themeInterface) =>
                      theme.palette.primary.main,
                  }}
                  onClick={() => handleRemove(p.id)}
                >
                  <CloseIcon />
                </IconButton>
              </AvatarBox>
            </Grid>
          ))}
        </Grid>
        {choosenPokemons.length === 2 && (
          <Button sx={{ width: 250 }} onClick={handleCompareClick}>
            Compare
          </Button>
        )}
      </Box>
    </Grid>
  );
};

export default ChoosenPokemonsBox;
