import React from "react";

import { Dialog, Box, Button } from "@mui/material";

type ModalProps = {
  open: boolean;
  onClose: () => void;
  children: JSX.Element;
};
const Modal = ({ open, onClose, children }: ModalProps) => {
  const handleCloseModal = () => {
    onClose();
  };

  return (
    <Dialog open={open} onClose={handleCloseModal} onClick={handleCloseModal}>
      <Box
        sx={{
          py: 4,
          px: 8,
          display: "flex",
          flexDirection: "column",
        }}
      >
        {children}
        <Button variant="contained">Close</Button>
      </Box>
    </Dialog>
  );
};

export default Modal;
