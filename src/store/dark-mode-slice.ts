import { createSlice } from "@reduxjs/toolkit";

type initialStateType = {
  darkMode: boolean;
};
const initialState: initialStateType = {
  darkMode: false,
};
const darkModeSlice = createSlice({
  name: "darkMode",
  initialState,
  reducers: {
    setMode: (state, action) => {
      state.darkMode = action.payload;
      console.log(action.payload);
      localStorage.setItem("darkMode", action.payload);
    },
  },
});

export const { setMode } = darkModeSlice.actions;

export default darkModeSlice.reducer;
