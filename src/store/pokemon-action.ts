import { pokemonAPI } from "../api/pokemon-api";
import { AppDispatch } from "./store";
import {
  addPokemonToCompareSuccess,
  getAllPokemonsWithDetailsSuccess,
  getAllPokemonsWithDetailsStarted,
  getAllPokemonsWithDetailsError,
  setPokemonsCount,
} from "./pokemon-slice";
import { pokemonWithDetailType } from "../types/pokemonTypes";

export const getAllPokemons =
  (count: number, from: number) => async (dispatch: AppDispatch) => {
    try {
      dispatch(getAllPokemonsWithDetailsStarted());
      //api res
      const pokemons = await pokemonAPI.getAllPokemons(count, from);

      if (!Array.isArray(pokemons)) {
        throw new Error("Unexpected response from API");
      }
      const updatedPokeList: pokemonWithDetailType[] = await Promise.all(
        pokemons.map(async (p: any) => {
          const pokemonDetail = await pokemonAPI.getDetails(p.url);
          return pokemonDetail;
        })
      );
      //disc
      dispatch(getAllPokemonsWithDetailsSuccess(updatedPokeList));
    } catch (err) {
      console.log(err);
      dispatch(getAllPokemonsWithDetailsError());
      throw err;
    }
  };

export const getAllPokemonsCount = () => async (dispatch: AppDispatch) => {
  try {
    const pokemonsCount = await pokemonAPI.getAllPokemonsCount();
    console.log(pokemonsCount);
    dispatch(setPokemonsCount(pokemonsCount));
  } catch (err) {
    throw err;
  }
};

export const addPokemonToCompare =
  (id: number) => async (dispatch: AppDispatch) => {
    try {
      const pokemon = await pokemonAPI.getDetails(
        "https://pokeapi.co/api/v2/pokemon/" + id
      );
      dispatch(addPokemonToCompareSuccess(pokemon));
    } catch (err) {
      throw err;
    }
  };
