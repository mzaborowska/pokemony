import { createSlice } from "@reduxjs/toolkit";
import { pokemonWithDetailType } from "../types/pokemonTypes";

type initialStateType = {
  pokemonsWithDetails: pokemonWithDetailType[];
  pokemonsToCompare: pokemonWithDetailType[];
  loadingPokemonsData: boolean;
  allPokemonsCount: number;
};
const initialState: initialStateType = {
  pokemonsWithDetails: [],
  pokemonsToCompare: [],
  loadingPokemonsData: false,
  allPokemonsCount: 0,
};

const pokemonSlice = createSlice({
  name: "pokemon",
  initialState,
  reducers: {
    setPokemonsCount: (state, action) => {
      state.allPokemonsCount = action.payload;
    },
    getAllPokemonsWithDetailsStarted: (state) => {
      state.loadingPokemonsData = true;
    },
    getAllPokemonsWithDetailsError: (state) => {
      state.loadingPokemonsData = false;
    },

    getAllPokemonsWithDetailsSuccess: (state, action) => {
      state.pokemonsWithDetails = action.payload;
      state.loadingPokemonsData = false;
    },
    addPokemonToCompareSuccess: (state, action) => {
      if (state.pokemonsToCompare.length === 2) {
        state.pokemonsToCompare = [state.pokemonsToCompare[1], action.payload];
      } else {
        state.pokemonsToCompare.push(action.payload);
      }
    },
    removePokemonFromCompareSuccess: (state, action): void => {
      const updatedPokemons = state.pokemonsToCompare.filter(
        (p) => p.id !== action.payload
      );
      state.pokemonsToCompare = updatedPokemons;
    },
  },
});

export const {
  addPokemonToCompareSuccess,
  removePokemonFromCompareSuccess,
  getAllPokemonsWithDetailsSuccess,
  getAllPokemonsWithDetailsStarted,
  getAllPokemonsWithDetailsError,
  setPokemonsCount,
} = pokemonSlice.actions;

export default pokemonSlice.reducer;
