import axios from "axios";
import {
  pokeTypesType,
  pokeAbilityType,
  pokeStatsType,
  pokemonWithDetailType,
} from "../types/pokemonTypes";

const baseURL = "https://pokeapi.co/api/v2/pokemon/";

export const pokemonAPI = {
  getAllPokemonsCount: async () => {
    try {
      const response = await axios.get(baseURL);
      return response.data.count;
    } catch (err) {
      return false;
    }
  },
  getAllPokemons: async (limit: number, offset: number) => {
    try {
      const pokemons: { id: number; name: string; url: string }[] = [];
      const response = await axios.get(baseURL, {
        params: { limit: limit, offset: offset },
      });
      console.log("getAllPokemons", response);
      response.data.results.forEach((data: any, index: number) => {
        pokemons.push({
          id: index,
          name: data.name[0].toUpperCase() + data.name.slice(1),
          url: data.url,
        });
      });
      //
      return pokemons;
    } catch (err) {
      console.log("ERR", err);
      return false;
    }
  },
  getDetails: async (url: string) => {
    try {
      const pokemonDetails: any | pokemonWithDetailType = {};
      const response = await axios.get(url);
      const responseData = response.data;
      pokemonDetails.name =
        responseData.name[0].toUpperCase() + responseData.name.slice(1);
      pokemonDetails.id = responseData.id;
      pokemonDetails.imgUrl = {
        front: responseData.sprites.front_default,
        back: responseData.sprites.back_default,
        dream: responseData.sprites.other.dream_world.front_default,
      };
      //types
      const types: string[] = [];
      responseData.types.forEach((type: pokeTypesType) => {
        types.push(type.type.name);
      });
      pokemonDetails.types = types;
      //abilities
      const abilities: string[] = [];
      responseData.abilities.forEach((ability: pokeAbilityType) => {
        abilities.push(ability.ability.name);
      });
      pokemonDetails.abilities = abilities;

      pokemonDetails.weight = responseData.weight;
      pokemonDetails.height = responseData.height;

      ///////=====================
      responseData.stats.forEach((stat: pokeStatsType) => {
        if (stat.stat.name === "hp") pokemonDetails.hp = stat.base_stat;
        if (stat.stat.name === "attack") pokemonDetails.attack = stat.base_stat;
        if (stat.stat.name === "defense")
          pokemonDetails.defense = stat.base_stat;
        if (stat.stat.name === "special-attack")
          pokemonDetails.specialAttack = stat.base_stat;
        if (stat.stat.name === "special-defense")
          pokemonDetails.specialDefense = stat.base_stat;
        if (stat.stat.name === "speed") pokemonDetails.speed = stat.base_stat;
      });
      pokemonDetails.moves = [];
      responseData.moves.forEach((moveData: any) => {
        pokemonDetails.moves.push(moveData.move.name);
      });
      console.log(pokemonDetails);

      return pokemonDetails;
    } catch (err) {
      return false;
    }
  },
};
