import React from "react";
import HomeTable from "../Components/Table/HomeTable";
import ChoosenPokemonsBox from "../Components/ChoosenPokemonsBox/ChoosenPokemonsBox";

const HomePage = () => {
  return (
    <div>
      <ChoosenPokemonsBox />
      <HomeTable />
    </div>
  );
};

export default HomePage;
