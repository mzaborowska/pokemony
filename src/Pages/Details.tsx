import React, { useEffect, useState } from "react";
import DetailsForm from "../Components/DetailsForm/DetailsForm";
import { useParams } from "react-router-dom";
import { pokemonAPI } from "../api/pokemon-api";
import { CircularProgress, Grid } from "@mui/material";
import { pokemonWithDetailType } from "../types/pokemonTypes";
import { Container } from "@mui/system";

const DetailsPage = () => {
  const { pokeId } = useParams();
  const [pokemon, setPokemon] = useState<pokemonWithDetailType | null>(null);

  useEffect(() => {
    const fetchPokemon = async () => {
      const res = await pokemonAPI.getDetails(
        "https://pokeapi.co/api/v2/pokemon/" + pokeId
      );
      console.log(res);
      setPokemon(res);
    };
    fetchPokemon();
  }, [pokeId]);
  return (
    <Container>
      {pokemon && <DetailsForm pokemon={pokemon} />}
      {!pokemon && (
        <Grid container justifyContent="center" p={16}>
          <CircularProgress />
        </Grid>
      )}
    </Container>
  );
};

export default DetailsPage;
