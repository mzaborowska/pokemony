import React, { useEffect } from "react";
import MainNavigation from "../Components/Navigation/MainNavigation";
import { Outlet } from "react-router-dom";
import { Container, Stack } from "@mui/material";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useAppDispatch, useAppSelector } from "../store/store";
import { setMode } from "../store/dark-mode-slice";

const Root = () => {
  const darkMode = useAppSelector((state) => state.darkMode.darkMode);

  const dispatch = useAppDispatch();
  useEffect(() => {
    const mode = localStorage.getItem("darkMode");
    dispatch(setMode(mode === "true"));
  }, []);

  return (
    <Stack>
      <MainNavigation />
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme={darkMode ? "dark" : "light"}
      />
      <Container>
        <Outlet />
      </Container>
    </Stack>
  );
};

export default Root;
