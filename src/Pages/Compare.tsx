import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../store/store";
import { useNavigate } from "react-router-dom";
import { Container, Grid } from "@mui/material";
import CompareForm from "../Components/CompareForm/CompareForm";

const ComparePage = () => {
  const pokemonsToCompare = useSelector(
    (state: RootState) => state.pokemon.pokemonsToCompare
  );

  const navigate = useNavigate();

  useEffect(() => {
    if (pokemonsToCompare.length !== 2) {
      navigate("/");
    }
  }, [pokemonsToCompare, navigate]);

  return (
    <Container sx={{ my: 8 }}>
      <Grid container spacing={8}>
        <Grid item xs={6}>
          <CompareForm data={pokemonsToCompare[0]} />
        </Grid>
        <Grid item xs={6}>
          <CompareForm data={pokemonsToCompare[1]} />
        </Grid>
      </Grid>
    </Container>
  );
};

export default ComparePage;
