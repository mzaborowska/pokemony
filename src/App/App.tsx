import React from "react";

import { RouterProvider } from "react-router-dom";
import applicationRouter from "./Router/ApplicationRouter";
import { useAppSelector } from "../store/store";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { pink, purple } from "@mui/material/colors";
import { CssBaseline } from "@mui/material";

const themeLight = createTheme({
  palette: {
    primary: {
      main: pink[500],
    },
    secondary: {
      main: purple[500],
    },
    contrastThreshold: 3,
  },
});
const themeDark = createTheme({
  palette: {
    primary: {
      main: pink[500],
    },
    secondary: {
      main: purple[500],
    },
    mode: "dark",
    contrastThreshold: 3,
  },
});

function App() {
  const darkMode = useAppSelector((state) => state.darkMode.darkMode);

  return (
    <>
      <ThemeProvider theme={!darkMode ? themeLight : themeDark}>
        <CssBaseline />
        <RouterProvider router={applicationRouter} />
      </ThemeProvider>
    </>
  );
}

export default App;
