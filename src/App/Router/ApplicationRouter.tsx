import React from "react";
import { createBrowserRouter } from "react-router-dom";

import RootPage from "../../Pages/Root";
import HomePage from "../../Pages/Home";
import DetailsPage from "../../Pages/Details";
import ComparePage from "../../Pages/Compare";

const applicationRouter = createBrowserRouter([
	{
		path: "/",
		element: <RootPage />,
		children: [
			{ path: "", element: <HomePage /> },
			{ path: "details/:pokeId", element: <DetailsPage /> },
			{ path: "compare", element: <ComparePage /> },
		],
	},
]);

export default applicationRouter;
