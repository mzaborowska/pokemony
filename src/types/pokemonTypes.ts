export type pokemonWithDetailType = {
  name: string;
  abilities: string[];
  height: number;
  weight: number;
  id: number;
  imgUrl: { front: string; back: string; dream: string };
  types: string[];
  moves: string[];

  hp: string;
  speed: number;
  attack: number;
  defense: number;
  specialAttack: number;
  specialDefense: number;
};

export type pokeTypesType = {
  slot: number;
  type: { name: string; url: string };
};
export type pokeAbilityType = {
  ability: { name: string; url: string };
  is_hidden: boolean;
  slot: number;
};

export type pokeStatsType = {
  base_stat: number;
  effort: number;
  stat: {
    name: string;
    url: string;
  };
};
